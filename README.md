Analysis MPTP+drug
================
Joanes Grandjean

## load environement and libraries

``` r
library(tidyverse,warn.conflicts = FALSE)
library(glue)
library(ggpubr)
library(datawizard)
```

    ## 
    ## Attaching package: 'datawizard'

    ## The following object is masked from 'package:tidyr':
    ## 
    ##     extract

``` r
library(parameters)
library(performance)
library(wesanderson)
```

## Load study file

``` r
gt_level<-c('veh_ctr','veh_KH', 'MPTP_ctr', 'MPTP_KH')

df<-read_tsv('assets/table/participants.tsv',col_types = cols()) %>% mutate(group=factor(.$group, levels=c('ctr','KH'))) %>% 
  mutate(treatment=factor(.$treatment, levels=c('veh','MPTP'))) %>% mutate(group_treatment=as_factor(glue('{.$treatment}_{.$group}'))) %>%  mutate(group_treatment =fct_relevel(.$group_treatment,gt_level))
```

    ## New names:
    ## • `Fecal_drops_d1` -> `Fecal_drops_d1...16`
    ## • `Fecal_drops_d1` -> `Fecal_drops_d1...22`

``` r
roi_list<-read_csv('assets/table/roi_label_clean.csv',col_types = cols())

my_comparisons <-list(c(1,4),c(1,3),c(1,2),c(3,4))
```

## Do the dual regression stats first.

``` r
#inter_coef<-c()
#inter_lower<-c()
#inter_upper<-c()
#inter_t<-c()
#inter_p<-c()

#for(dr_file in dir(path = 'assets/DR/')){
#   df$dr<-read_delim(glue('assets/DR/{dr_file}'),col_names = c('dr','a'),delim = '  ',show_col_types = FALSE) %>% data_extract('dr')
#   mod_param<- lm(dr~group*treatment,df) %>% model_parameters(standardize = "refit")
#   inter_coef<-c(inter_coef,mod_param$Coefficient[4])
#   inter_lower<-c(inter_lower,mod_param$CI_low[4])
#   inter_upper<-c(inter_upper,mod_param$CI_high[4])
#   inter_t<-c(inter_t,mod_param$t[4])
#   inter_p<-c(inter_p,mod_param$p[4])
# }
# 
# 
# tibble(inter_coef,inter_lower,inter_upper,inter_t,inter_p) %>% write_tsv('assets/table/DR_stats.tsv')


df$dr<-read_delim(glue('assets/DR/00dr_stage2_ic0008.txt'),col_names = c('dr','a'),delim = '  ',show_col_types = FALSE) %>% data_extract('dr')
b<-ggboxplot(df, x = "group_treatment", y = "dr",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='Functional connectivity [z-value]') +labs_pubr(base_size = 6) 

df$dr<-read_delim(glue('assets/DR/00dr_stage2_ic0012.txt'),col_names = c('dr','a'),delim = '  ',show_col_types = FALSE) %>% data_extract('dr')
d<-ggboxplot(df, x = "group_treatment", y = "dr",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons,method = "t.test",size = 2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='Functional connectivity [z-value]')  +labs_pubr(base_size = 6)

a<-c()
c<-c()
fig_dr<-ggarrange(a, b,c,d, ncol = 2, nrow = 2,labels='auto',font.label = list(size = 8, face = "bold"),widths=c(0.4,0.6))
```

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.
    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

``` r
fig_dr %>%   ggsave('assets/figure/dr.svg',plot = .,device = 'svg',units = 'mm',width = 120,height = 100,dpi=300)
fig_dr %>%   ggsave('assets/figure/dr.png',plot = .,device = 'png',units = 'mm',width = 120,height = 100,dpi=300)
```

## Do the DTI - FA stats.

``` r
fa<-read_delim('assets/DTI/FA_roi',delim = '  ',col_names = FALSE,col_types = cols()) %>% select(!X357)
md<-read_delim('assets/DTI/MD_roi',delim = '  ',col_names = FALSE,col_types = cols()) %>% select(!X357)

# 
# inter_coef<-c()
# inter_lower<-c()
# inter_upper<-c()
# inter_t<-c()
# inter_p<-c()
# 
# for(i in roi_list$label){
#   df$fa<- data_extract(fa,names(fa)[i])
#   mod_param<- lm(fa~group*treatment,df) %>% model_parameters(standardize = "refit")
#   inter_coef<-c(inter_coef,mod_param$Coefficient[4])
#   inter_lower<-c(inter_lower,mod_param$CI_low[4])
#   inter_upper<-c(inter_upper,mod_param$CI_high[4])
#   inter_t<-c(inter_t,mod_param$t[4])
#   inter_p<-c(inter_p,mod_param$p[4])
# }


# fa_stats<-roi_list %>% add_column(inter_coef,inter_lower,inter_upper,inter_t,inter_p) 
# fa_stats %>% write_tsv('assets/table/FA_stats.tsv')

df$fa_cc_left<- data_extract(fa,names(fa)[68])
df$fa_cc_right<- data_extract(fa,names(fa)[8])
df$md_cc_left<- data_extract(md,names(fa)[68])
df$md_cc_right<- data_extract(md,names(fa)[8])

b<-ggboxplot(df, x = "group_treatment", y = "fa_cc_left",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='FA left corpus callosum [a.u.]') +labs_pubr(base_size = 6) 

c<-ggboxplot(df, x = "group_treatment", y = "fa_cc_right",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons,method = "t.test",size = 2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='FA right corpus callosum [a.u.]')  +labs_pubr(base_size = 6)

e<-ggboxplot(df, x = "group_treatment", y = "md_cc_left",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='MD left corpus callosum [a.u.]') +labs_pubr(base_size = 6) 

f<-ggboxplot(df, x = "group_treatment", y = "md_cc_right",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons,method = "t.test",size = 2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='MD right corpus callosum [a.u.]')  +labs_pubr(base_size = 6)

a<-c()
d<-c()
fig_fa<-ggarrange(a, b,c,d,e,f, ncol = 3, nrow = 2,labels='auto',font.label = list(size = 8, face = "bold"))
```

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.
    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.
    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.
    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

``` r
fig_fa %>%   ggsave('assets/figure/fa.svg',plot = .,device = 'svg',units = 'mm',width = 190,height = 100,dpi=300)
fig_fa %>%   ggsave('assets/figure/fa.png',plot = .,device = 'png',units = 'mm',width = 190,height = 100,dpi=300)
```

## Do the DTI - MD stats.

``` r
md<-read_delim('assets/DTI/MD_roi',delim = '  ',col_names = FALSE,col_types = cols()) %>% select(!X357)

# 
# inter_coef<-c()
# inter_lower<-c()
# inter_upper<-c()
# inter_t<-c()
# inter_p<-c()
# 
# for(i in roi_list$label){
#   df$md<- data_extract(md,names(md)[i])
#   mod_param<- lm(md~group*treatment,df) %>% model_parameters(standardize = "refit")
#   inter_coef<-c(inter_coef,mod_param$Coefficient[4])
#   inter_lower<-c(inter_lower,mod_param$CI_low[4])
#   inter_upper<-c(inter_upper,mod_param$CI_high[4])
#   inter_t<-c(inter_t,mod_param$t[4])
#   inter_p<-c(inter_p,mod_param$p[4])
# }
# 
# 
# md_stats<-roi_list %>% add_column(inter_coef,inter_lower,inter_upper,inter_t,inter_p) 
# md_stats %>% write_tsv('assets/table/md_stats.tsv')
# 
# md_stats[which(md_stats$inter_p<0.05),]
```

## Do the SPECT data.

``` r
#striatum / cerebellum ratio doesn't seem like a good idea. it amplifies outliers
df$spect_ratio<-df$spect_striatum/df$spect_cerebellum
df$spect_striatum_corrected<-df$spect_striatum / ((df$injected_radioactivity - df$radioactivity_left_syringe)/df$weight_SPECT)

mod_param_ratio<- lm(spect_striatum_corrected~group*treatment,df) %>% model_parameters(standardize = "refit")


b<-ggboxplot(df, x = "group_treatment", y = "spect_striatum_corrected",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='SUV corrected striatum [activity / gram]') +labs_pubr(base_size = 6) 

c<-ggboxplot(df, x = "group_treatment", y = "spect_ratio",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='SUV striatum/cerebellum [a.u.]') +labs_pubr(base_size = 6) 




a<-c()

fig_spect<-ggarrange(a, b, ncol = 2, nrow = 1,labels='auto',font.label = list(size = 8, face = "bold"))
```

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

``` r
fig_spect %>%   ggsave('assets/figure/spect.svg',plot = .,device = 'svg',units = 'mm',width = 120,height = 60,dpi=300)
fig_spect %>%   ggsave('assets/figure/spect.png',plot = .,device = 'png',units = 'mm',width = 120,height = 60,dpi=300)
```

## do the behaviour

``` r
#lm(Open_field_total_d1~group*treatment,df) %>% model_parameters(standardize = "refit")
# lm(Open_field_velocity_d1~group*treatment,df) %>% model_parameters(standardize = "refit")
# lm(Open_field_frequency_d1~group*treatment,df) %>% model_parameters(standardize = "refit")
# lm(Open_field_cummulative_d1~group*treatment,df) %>% model_parameters(standardize = "refit")
# lm(Open_field_fecal_d1~group*treatment,df) %>% model_parameters(standardize = "refit")
# lm(Open_field_f5min_d1~group*treatment,df) %>% model_parameters(standardize = "refit")
# 
# lm(Open_field_total_d2~group*treatment,df) %>% model_parameters(standardize = "refit")
# lm(Open_field_velocity_d2~group*treatment,df) %>% model_parameters(standardize = "refit")
# lm(Open_field_frequency_d2~group*treatment,df) %>% model_parameters(standardize = "refit")
# lm(Open_field_cummulative_d2~group*treatment,df) %>% model_parameters(standardize = "refit")
# lm(Open_field_fecal_d2~group*treatment,df) %>% model_parameters(standardize = "refit")
# lm(Open_field_f5min_d2~group*treatment,df) %>% model_parameters(standardize = "refit")


a<-ggboxplot(df, x = "group_treatment", y = "Total_distance_d1",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='Open field total movement [cm]') +labs_pubr(base_size = 6) 

b<-ggboxplot(df, x = "group_treatment", y = "Velocity_d1",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='Open field average velocity [cm/s]') +labs_pubr(base_size = 6) 

c<-ggboxplot(df, x = "group_treatment", y = "Freq_center_d1",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='Open field frequency in center [a.u.]') +labs_pubr(base_size = 6) 

d<-ggboxplot(df, x = "group_treatment", y = "Duration_center_d1",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='Open field duration center [s]') +labs_pubr(base_size = 6) 

fig_of<-ggarrange(a, b, c,d,ncol = 2, nrow = 2,labels='auto',font.label = list(size = 8, face = "bold"))
```

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.
    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.
    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.
    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

``` r
fig_of %>%   ggsave('assets/figure/of.svg',plot = .,device = 'svg',units = 'mm',width = 120,height = 100,dpi=300)
fig_of %>%   ggsave('assets/figure/of.png',plot = .,device = 'png',units = 'mm',width = 120,height = 120,dpi=300)
```

``` r
a<-ggboxplot(df, x = "group_treatment", y = "Rotarod_latency_trial1",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='Rotarod latency trial [a.u.]') +labs_pubr(base_size = 6) 

b<-ggboxplot(df, x = "group_treatment", y = "Rotarod_rpm_trial1",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='Rotarod rpm trial [a.u.]') +labs_pubr(base_size = 6) 


fig_rota<-ggarrange(a,b, ncol = 2, nrow = 1,labels='auto',font.label = list(size = 8, face = "bold"))
```

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.
    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

``` r
fig_rota %>%   ggsave('assets/figure/rota.svg',plot = .,device = 'svg',units = 'mm',width = 90,height = 60,dpi=300)
fig_rota %>%   ggsave('assets/figure/rota.png',plot = .,device = 'png',units = 'mm',width = 90,height = 60,dpi=300)
```

``` r
a<-ggboxplot(df, x = "group_treatment", y = "FC_EXT1_first3min",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='FC ext1 first 3 min [a.u.]') +labs_pubr(base_size = 6) 

b<-ggboxplot(df, x = "group_treatment", y = "FC_EXT1_sec3min",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='FC ext1 second 3 min [a.u.]') +labs_pubr(base_size = 6) 


fig_fc<-ggarrange(a, b,ncol = 2, nrow =1,labels='auto',font.label = list(size = 8, face = "bold"))
```

    ## Warning: Removed 7 rows containing non-finite values (stat_boxplot).

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

    ## Warning: Removed 7 rows containing non-finite values (stat_bindot).

    ## Warning: Removed 7 rows containing non-finite values (stat_signif).

    ## Warning: Removed 7 rows containing non-finite values (stat_compare_means).

    ## Warning: Removed 6 rows containing non-finite values (stat_boxplot).

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

    ## Warning: Removed 6 rows containing non-finite values (stat_bindot).

    ## Warning: Removed 6 rows containing non-finite values (stat_signif).

    ## Warning: Removed 6 rows containing non-finite values (stat_compare_means).

``` r
fig_fc %>%   ggsave('assets/figure/fc.svg',plot = .,device = 'svg',units = 'mm',width = 90,height = 60,dpi=300)
fig_fc %>%   ggsave('assets/figure/fc.png',plot = .,device = 'png',units = 'mm',width = 90,height = 60,dpi=300)
```

``` r
a<-ggboxplot(df, x = "group_treatment", y = "Grip_test_for+hindlimbs_mean",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='Grip test hindlimb max [a.u.]') +labs_pubr(base_size = 6) 

b<-ggboxplot(df, x = "group_treatment", y = "Grip_test_folimbs_mean",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='Grip test forelimb max  [a.u.]') +labs_pubr(base_size = 6) 



fig_grip<-ggarrange(a, b,ncol = 2, nrow =1,labels='auto',font.label = list(size = 8, face = "bold"))
```

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.
    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

``` r
fig_grip %>%   ggsave('assets/figure/grip.svg',plot = .,device = 'svg',units = 'mm',width = 90,height = 60,dpi=300)
fig_grip %>%   ggsave('assets/figure/grip.png',plot = .,device = 'png',units = 'mm',width = 90,height = 60,dpi=300)
```

``` r
a<-ggboxplot(df, x = "group_treatment", y = "Catwalk_run_duration",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='Catwalk run duration [s]') +labs_pubr(base_size = 6) 

b<-ggboxplot(df, x = "group_treatment", y = "Catwalk_run_speed",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='Catwalk run speed  [cm/s]') +labs_pubr(base_size = 6) 

c<-ggboxplot(df, x = "group_treatment", y = "Catwalk_run_variation",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='Catwalk run variation  [a.u.]') +labs_pubr(base_size = 6) 


fig_cat<-ggarrange(a, b,c,ncol = 3, nrow =1,labels='auto',font.label = list(size = 8, face = "bold"))
```

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.
    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.
    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

``` r
fig_cat %>%   ggsave('assets/figure/cat.svg',plot = .,device = 'svg',units = 'mm',width = 120,height = 60,dpi=300)
fig_cat %>%   ggsave('assets/figure/cat.png',plot = .,device = 'png',units = 'mm',width = 120,height = 60,dpi=300)
```

``` r
a<-ggboxplot(df, x = "treatment", y = "TH",
          color = "treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(method = "t.test",size=2)+ labs(x='groups', y='TH staining [a.u.]') +labs_pubr(base_size = 6) 

b<-ggboxplot(df, x = "treatment", y = "Alpha_synuclein_aggregate",
          color = "treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means( method = "t.test",size=2)+ labs(x='groups', y='Alpha synuclein aggregate  [a.u.]') +labs_pubr(base_size = 6) 

c<-ggboxplot(df, x = "treatment", y = "8-OH-dG",
          color = "treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(method = "t.test",size=2) + labs(x='groups', y='8-OH-dG [a.u.]') +labs_pubr(base_size = 6) 



fig_histo<-ggarrange(a,b,c, ncol = 3, nrow = 1,labels='auto',font.label = list(size = 8, face = "bold"))
```

    ## Warning: Removed 20 rows containing non-finite values (stat_boxplot).

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

    ## Warning: Removed 20 rows containing non-finite values (stat_bindot).

    ## Warning: Removed 20 rows containing non-finite values (stat_compare_means).

    ## Warning: Removed 20 rows containing non-finite values (stat_boxplot).

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

    ## Warning: Removed 20 rows containing non-finite values (stat_bindot).

    ## Warning: Removed 20 rows containing non-finite values (stat_compare_means).

    ## Warning: Removed 20 rows containing non-finite values (stat_boxplot).

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

    ## Warning: Removed 20 rows containing non-finite values (stat_bindot).

    ## Warning: Removed 20 rows containing non-finite values (stat_compare_means).

``` r
fig_histo %>%   ggsave('assets/figure/histo.svg',plot = .,device = 'svg',units = 'mm',width = 120,height = 60,dpi=300)
fig_histo %>%   ggsave('assets/figure/histo.png',plot = .,device = 'png',units = 'mm',width = 120,height = 60,dpi=300)
```

``` r
a<-ggboxplot(df, x = "treatment", y = "KH176_plasma",
          color = "treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means( method = "t.test",size=2)+ labs(x='groups', y='KH176 in plasma [a.u.]') +labs_pubr(base_size = 6) 

b<-ggboxplot(df, x = "treatment", y = "KH176_striatum",
          color = "treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(method = "t.test",size=2) + labs(x='groups', y='KH176 in Striatum [a.u.]') +labs_pubr(base_size = 6) 



fig_kh<-ggarrange(a, b,ncol = 2, nrow =1,labels='auto',font.label = list(size = 8, face = "bold"))
```

    ## Warning: Removed 18 rows containing non-finite values (stat_boxplot).

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

    ## Warning: Removed 18 rows containing non-finite values (stat_bindot).

    ## Warning: Removed 18 rows containing non-finite values (stat_compare_means).

    ## Warning: Removed 17 rows containing non-finite values (stat_boxplot).

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

    ## Warning: Removed 17 rows containing non-finite values (stat_bindot).

    ## Warning: Removed 17 rows containing non-finite values (stat_compare_means).

``` r
fig_kh %>%   ggsave('assets/figure/kh.svg',plot = .,device = 'svg',units = 'mm',width = 90,height = 60,dpi=300)
fig_kh %>%   ggsave('assets/figure/kh.png',plot = .,device = 'png',units = 'mm',width = 90,height = 60,dpi=300)
```

``` r
a<-ggboxplot(df, x = "group_treatment", y = "DA_Striatum",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='DA in striatum [a.u.]') +labs_pubr(base_size = 6) 

b<-ggboxplot(df, x = "group_treatment", y = "DA_midbrain",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='DA in midbrain [a.u.]') +labs_pubr(base_size = 6) 

c<-ggboxplot(df, x = "group_treatment", y = "DA_frontalcortex",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='DA in frontal cortex [a.u.]') +labs_pubr(base_size = 6) 

d<-ggboxplot(df, x = "group_treatment", y = "DA_cerebellum",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='DA in cerebellum [a.u.]') +labs_pubr(base_size = 6) 

e<-ggboxplot(df, x = "group_treatment", y = "DOPAC_striatum",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='DOPAC in striatum [a.u.]') +labs_pubr(base_size = 6) 

f<-ggboxplot(df, x = "group_treatment", y = "DOPAC_midbrain",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='DOPAC in midbrain [a.u.]') +labs_pubr(base_size = 6) 
g<-ggboxplot(df, x = "group_treatment", y = "HVA_striatum",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='HVA in striatum [a.u.]') +labs_pubr(base_size = 6) 


i<-ggboxplot(df, x = "group_treatment", y = "Serotonin_Striatum",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='Serotonin in striatum [a.u.]') +labs_pubr(base_size = 6) 

j<-ggboxplot(df, x = "group_treatment", y = "5HIAA_striatum",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='5HIAA in striatum [a.u.]') +labs_pubr(base_size = 6) 
k<-ggboxplot(df, x = "group_treatment", y = "NA_striatum",
          color = "group_treatment", palette = wes_palette("Darjeeling1"), add = "dotplot",legend='none')+
          stat_compare_means(comparisons = my_comparisons, method = "t.test",size=2)+
          stat_compare_means(method = "anova",size=2) + labs(x='groups', y='NA in striatum [a.u.]') +labs_pubr(base_size = 6) 



fig_da<-ggarrange(a,e,g,i,j,k,ncol = 3, nrow =2,labels='auto',font.label = list(size = 8, face = "bold"))
```

    ## Warning: Removed 3 rows containing non-finite values (stat_boxplot).

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

    ## Warning: Removed 3 rows containing non-finite values (stat_bindot).

    ## Warning: Removed 3 rows containing non-finite values (stat_signif).

    ## Warning: Removed 3 rows containing non-finite values (stat_compare_means).

    ## Warning: Removed 3 rows containing non-finite values (stat_boxplot).

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

    ## Warning: Removed 3 rows containing non-finite values (stat_bindot).

    ## Warning: Removed 3 rows containing non-finite values (stat_signif).

    ## Warning: Removed 3 rows containing non-finite values (stat_compare_means).

    ## Warning: Removed 3 rows containing non-finite values (stat_boxplot).

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

    ## Warning: Removed 3 rows containing non-finite values (stat_bindot).

    ## Warning: Removed 3 rows containing non-finite values (stat_signif).

    ## Warning: Removed 3 rows containing non-finite values (stat_compare_means).

    ## Warning: Removed 3 rows containing non-finite values (stat_boxplot).

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

    ## Warning: Removed 3 rows containing non-finite values (stat_bindot).

    ## Warning: Removed 3 rows containing non-finite values (stat_signif).

    ## Warning: Removed 3 rows containing non-finite values (stat_compare_means).

    ## Warning: Removed 3 rows containing non-finite values (stat_boxplot).

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

    ## Warning: Removed 3 rows containing non-finite values (stat_bindot).

    ## Warning: Removed 3 rows containing non-finite values (stat_signif).

    ## Warning: Removed 3 rows containing non-finite values (stat_compare_means).

    ## Warning: Removed 3 rows containing non-finite values (stat_boxplot).

    ## Bin width defaults to 1/30 of the range of the data. Pick better value with `binwidth`.

    ## Warning: Removed 3 rows containing non-finite values (stat_bindot).

    ## Warning: Removed 3 rows containing non-finite values (stat_signif).

    ## Warning: Removed 3 rows containing non-finite values (stat_compare_means).

``` r
fig_da %>%   ggsave('assets/figure/da.svg',plot = .,device = 'svg',units = 'mm',width = 120,height = 120,dpi=300)
fig_da %>%   ggsave('assets/figure/da.png',plot = .,device = 'png',units = 'mm',width = 120,height = 120,dpi=300)
```
